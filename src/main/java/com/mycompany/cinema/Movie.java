/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.cinema;

import static com.mycompany.cinema.Showtime.kb;

/**
 *
 * @author Name
 */
public class Movie {
    static int number = 0;
    static String[] name = {"Barbie", "The Meg 2", "Long Live Love"};
    static String[] theatre = {"(Cinema : 1)", "(Cinema : 2)", "(Cinema : 3)"};
    public static void printMovie() {
        for (int i = 0; i < 3; i++) {
            System.out.println(i + 1 + ": " + name[i] + " " + theatre[i]);
        }
    }
    
    public static void selectMovie() {
        while (true) {
            System.out.print("Please Choose your Movie Number :");
            number = kb.nextInt();
            if (number == 1) {
                System.out.println("========Movie========");
                System.out.println(name[number - 1] + " " + theatre[number - 1]);
                Showtime.selectDate();
                break;
            } else if (number == 2) {
                System.out.println("========Movie========");
                System.out.println(name[number - 1] + " " + theatre[number - 1]);
                Showtime.selectDate();
                break;
            } else if (number == 3) {
                System.out.println("========Movie========");
                System.out.println(name[number - 1] + " " + theatre[number - 1]);
                Showtime.selectDate();
                break;
            } else {
                System.out.println("Movie is not found.");
                continue;
            }
        }
    }  
}


