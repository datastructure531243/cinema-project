/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.cinema;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Name
 */
public class ProjectCinema {
    static int people;

    static Scanner kb = new Scanner(System.in);

    static ArrayList<String> seatType = new ArrayList<>();

    public static void main(String[] args) {
        while (true) {
            printWelcome();
            Movie.printMovie();
            Movie.selectMovie();
            Ticket.PrintDetails();
            if (Seat.confirmBooking() == true) {
                Ticket.Finaldetails();
                Seat.clearBooking();
                seatType.clear();
            } else {
                Seat.clearBooking();
            }
        }
    }

    public static void printWelcome() {
        System.out.println("-----------------------");
        System.out.println(" Welcome to DS Cinema");
        System.out.println("-----------------------");
    }

    public static void seatType() {
        System.out.println("A-F is Deluxe seat 220 Baht");
        System.out.println("G-L is Normal seat 180 Baht");
    }

    public static void printSeatType() {
        System.out.print("Seat type: ");
        for (int i = 0; i < people; i++) {
            System.out.println(seatType.get(i) + " ");
        }
    }
}

