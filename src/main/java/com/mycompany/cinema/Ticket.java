/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.cinema;

import static com.mycompany.cinema.Movie.name;
import static com.mycompany.cinema.Movie.number;
import static com.mycompany.cinema.Movie.theatre;
import static com.mycompany.cinema.ProjectCinema.people;
import static com.mycompany.cinema.ProjectCinema.printSeatType;
import static com.mycompany.cinema.Seat.chosenSeat;
import java.util.ArrayList;

/**
 *
 * @author Name
 */

public class Ticket {
    static int priceTotal = 0;
    static ArrayList<String> DateBooking = new ArrayList<>();
    static ArrayList<String> TimeBooking = new ArrayList<>();
    
    public static void printSeat() {
        System.out.print("Your seat is: ");
        for(int i = 0 ; i < people ; i++){
            System.out.print(chosenSeat.get(i)+" ");
        }
        System.out.println("");
    }
    public static void printDateTime(){
        for (int i = 0; i < DateBooking.size(); i++) {
            System.out.print(DateBooking.get(i) + " Time: ");
            System.out.println(TimeBooking.get(i));
        }
    }
    public static void PrintDetails() {
        System.out.println("============================");
        if (number == 1) {
            System.out.println(name[number - 1] + " " + theatre[number - 1]);
        } else if (number == 2) {
            System.out.println(name[number - 1] + " " + theatre[number - 1]);
        } else if (number == 3) {
            System.out.println(name[number - 1] + " " + theatre[number - 1]);
        }
        printDateTime();
        printSeatType();
        printSeat();
        System.out.println("Total: "+priceTotal+" Baht");
    }
    public static void Finaldetails() {
        System.out.println("=============TICKET=============");
        if (number == 1) {
            System.out.println(name[number - 1] + " " + theatre[number - 1]);
        } else if (number == 2) {
            System.out.println(name[number - 1] + " " + theatre[number - 1]);
        } else if (number == 3) {
            System.out.println(name[number - 1] + " " + theatre[number - 1]);
        }
        printDateTime();
        printSeatType();
        printSeat();
        System.out.println("================================");
        priceTotal = 0;
    }
    
}

