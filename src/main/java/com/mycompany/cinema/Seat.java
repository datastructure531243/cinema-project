/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.cinema;

import static com.mycompany.cinema.Movie.number;
import static com.mycompany.cinema.ProjectCinema.people;
import static com.mycompany.cinema.ProjectCinema.seatType;
import static com.mycompany.cinema.Showtime.day;
import static com.mycompany.cinema.Showtime.kb;
import static com.mycompany.cinema.Showtime.round;
import static com.mycompany.cinema.Ticket.priceTotal;
import java.util.ArrayList;

/**
 *
 * @author Name
 */
public class Seat {

    static String[][] seats = {{"A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "A13", "A14", "A15", "A16", "A17", "A18", "A19", "A20"},
    {"B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10", "B11", "B12", "B13", "B14", "B15", "B16", "B17", "B18", "B19", "B20"},
    {"C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "C10", "C11", "C12", "C13", "C14", "C15", "C16", "C17", "C18", "C19", "C20"},
    {"D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "D10", "D11", "D12", "D13", "D14", "D15", "D16", "D17", "D18", "D19", "D20"},
    {"E1", "E2", "E3", "E4", "E5", "E6", "E7", "E8", "E9", "E10", "E11", "E12", "E13", "E14", "E15", "E16", "E17", "E18", "E19", "E20"},
    {"F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12", "F13", "F14", "F15", "F16", "F17", "F18", "F19", "F20"},
    {"G1", "G2", "G3", "G4", "G5", "G6", "G7", "G8", "G9", "G10", "G11", "G12", "G13", "G14", "G15", "G16", "G17", "G18", "G19", "G20"},
    {"H1", "H2", "H3", "H4", "H5", "H6", "H7", "H8", "H9", "H10", "H11", "H12", "H13", "H14", "H15", "H16", "H17", "H18", "H19", "H20"},
    {"I1", "I2", "I3", "I4", "I5", "I6", "I7", "I8", "I9", "I10", "I11", "I12", "I13", "I14", "I15", "I16", "I17", "I18", "I19", "I20"},
    {"J1", "J2", "J3", "J4", "J5", "J6", "J7", "J8", "J9", "J10", "J11", "J12", "J13", "J14", "J15", "J16", "J17", "J18", "J19", "J20"},
    {"K1", "K2", "K3", "K4", "K5", "K6", "K7", "K8", "K9", "K10", "K11", "K12", "K13", "K14", "K15", "K16", "K17", "K18", "K19", "K20"},
    {"L1", "L2", "L3", "L4", "L5", "L6", "L7", "L8", "L9", "L10", "L11", "L12", "L13", "L14", "L15", "L16", "L17", "L18", "L19", "L20"}
    };

    static String[][] Barbie_1_1 = seats;
    static String[][] Barbie_1_2 = seats;
    static String[][] Barbie_2_1 = seats;
    static String[][] Barbie_2_2 = seats;
    static String[][] TheMeg2_1_1 = seats;
    static String[][] TheMeg2_1_2 = seats;
    static String[][] TheMeg2_2_1 = seats;
    static String[][] TheMeg2_2_2 = seats;
    static String[][] LongLiveLove_1_1 = seats;
    static String[][] LongLiveLove_1_2 = seats;
    static String[][] LongLiveLove_2_1 = seats;
    static String[][] LongLiveLove_2_2 = seats;

    static ArrayList<String> chosenSeat = new ArrayList<>();
    static ArrayList<String> seatConfirm = new ArrayList<>();
    static ArrayList<String> seatConfirm111 = new ArrayList<>();
    static ArrayList<String> seatConfirm112 = new ArrayList<>();
    static ArrayList<String> seatConfirm121 = new ArrayList<>();
    static ArrayList<String> seatConfirm122 = new ArrayList<>();
    static ArrayList<String> seatConfirm211 = new ArrayList<>();
    static ArrayList<String> seatConfirm212 = new ArrayList<>();
    static ArrayList<String> seatConfirm221 = new ArrayList<>();
    static ArrayList<String> seatConfirm222 = new ArrayList<>();
    static ArrayList<String> seatConfirm311 = new ArrayList<>();
    static ArrayList<String> seatConfirm312 = new ArrayList<>();
    static ArrayList<String> seatConfirm321 = new ArrayList<>();
    static ArrayList<String> seatConfirm322 = new ArrayList<>();
    static String seat;

    static void displaySeats() {
        if (number == 1) {
            if (day.equals("28/08/2023")) {
                if (round.equals("10.00(TH)")) {
                    System.out.println("                             Barbie                                  ");
                    for (int i = 0; i < 12; i++) {
                        for (int j = 0; j < 20; j++) {
                            System.out.print(Barbie_1_1[i][j] + " ");
                        }
                        System.out.println("");
                    }
                } else if (round.equals("14.00(ENG)")) {
                    System.out.println("                             Barbie                                  ");
                    for (int i = 0; i < 12; i++) {
                        for (int j = 0; j < 20; j++) {
                            System.out.print(Barbie_1_2[i][j] + " ");
                        }
                        System.out.println("");
                    }
                }
            } else if (day.equals("29/08/2023")) {
                if (round.equals("10.00(TH)")) {
                    System.out.println("                             Barbie                                  ");
                    for (int i = 0; i < 12; i++) {
                        for (int j = 0; j < 20; j++) {
                            System.out.print(Barbie_2_1[i][j] + " ");
                        }
                        System.out.println("");
                    }
                } else if (round.equals("14.00(ENG)")) {
                    System.out.println("                             Barbie                                  ");
                    for (int i = 0; i < 12; i++) {
                        for (int j = 0; j < 20; j++) {
                            System.out.print(Barbie_2_2[i][j] + " ");
                        }
                        System.out.println("");
                    }
                }
            }
        } else if (number == 2) {
            if (day.equals("28/08/2023")) {
                if (round.equals("10.00(TH)")) {
                    System.out.println("                             The Meg2                          ");
                    for (int i = 0; i < 12; i++) {
                        for (int j = 0; j < 20; j++) {
                            System.out.print(TheMeg2_1_1[i][j] + " ");
                        }
                        System.out.println("");
                    }
                } else if (round.equals("14.00(ENG)")) {
                    System.out.println("                             The Meg2                          ");
                    for (int i = 0; i < 12; i++) {
                        for (int j = 0; j < 20; j++) {
                            System.out.print(TheMeg2_1_2[i][j] + " ");
                        }
                        System.out.println("");
                    }
                }
            } else if (day.equals("29/08/2023")) {
                if (round.equals("10.00(TH)")) {
                    System.out.println("                             The Meg2                          ");
                    for (int i = 0; i < 12; i++) {
                        for (int j = 0; j < 20; j++) {
                            System.out.print(TheMeg2_2_1[i][j] + " ");
                        }
                        System.out.println("");
                    }
                } else if (round.equals("14.00(ENG)")) {
                    System.out.println("                             The Meg2                          ");
                    for (int i = 0; i < 12; i++) {
                        for (int j = 0; j < 20; j++) {
                            System.out.print(TheMeg2_2_2[i][j] + " ");
                        }
                        System.out.println("");
                    }
                }
            }
        } else if (number == 3) {
            if (day.equals("28/08/2023")) {
                if (round.equals("10.00(TH)")) {
                    System.out.println("                             LongLiveLove                     ");
                    for (int i = 0; i < 12; i++) {
                        for (int j = 0; j < 20; j++) {
                            System.out.print(LongLiveLove_1_1[i][j] + " ");
                        }
                        System.out.println("");
                    }
                } else if (round.equals("14.00(ENG)")) {
                    System.out.println("                             LongLiveLove                                  ");
                    for (int i = 0; i < 12; i++) {
                        for (int j = 0; j < 20; j++) {
                            System.out.print(LongLiveLove_1_2[i][j] + " ");
                        }
                        System.out.println("");
                    }
                }
            } else if (day.equals("29/08/2023")) {
                if (round.equals("10.00(TH)")) {
                    System.out.println("                             LongLiveLove                                  ");
                    for (int i = 0; i < 12; i++) {
                        for (int j = 0; j < 20; j++) {
                            System.out.print(LongLiveLove_2_1[i][j] + " ");
                        }
                        System.out.println("");
                    }
                } else if (round.equals("14.00(ENG)")) {
                    System.out.println("                             LongLiveLove                                  ");
                    for (int i = 0; i < 12; i++) {
                        for (int j = 0; j < 20; j++) {
                            System.out.print(LongLiveLove_2_2[i][j] + " ");
                        }
                        System.out.println("");
                    }
                }
            }
        }
    }

    public static void selectSeat() {
        System.out.print("Please select number of people :");
        people = kb.nextInt();
        displaySeats();
        System.out.println("");
        System.out.println("                             Monitor                                  ");
        System.out.println("----------------------------------------------------------------------");
        seatType();
        System.out.print("Please select your seat:");
        for (int p = 0; p < people; p++) {
            seat = kb.next();
            if (number == 1) {
                if (day.equals("28/08/2023")) {
                    if (round.equals("10.00(TH)")) {
                        for (int i = 0; i < 12; i++) {
                            for (int j = 0; j < 20; j++) {
                                while (seatConfirm111.contains(seat.toUpperCase())) {
                                    System.out.println("This seat is selected already.");
                                    System.out.print("Please select a new seat :");
                                    seat = kb.next();
                                    if (seatConfirm111.contains(seat.toUpperCase())) {
                                        continue;
                                    } else {
                                        chosenSeat.add(seat.toUpperCase());
                                        break;
                                    }
                                }
                            }
                        }
                    } else if (round.equals("14.00(ENG)")) {
                        for (int i = 0; i < 12; i++) {
                            for (int j = 0; j < 20; j++) {
                                while (seatConfirm112.contains(seat.toUpperCase())) {
                                    System.out.println("This seat is selected already.");
                                    System.out.print("Please select a new seat :");
                                    seat = kb.next();
                                    if (seatConfirm112.contains(seat.toUpperCase())) {
                                        continue;
                                    } else {
                                        chosenSeat.add(seat.toUpperCase());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                } else if (day.equals("29/08/2023")) {
                    if (round.equals("10.00(TH)")) {
                        for (int i = 0; i < 12; i++) {
                            for (int j = 0; j < 20; j++) {
                                while (seatConfirm121.contains(seat.toUpperCase())) {
                                    System.out.println("This seat is selected already.");
                                    System.out.print("Please select a new seat :");
                                    seat = kb.next();
                                    if (seatConfirm121.contains(seat.toUpperCase())) {
                                        continue;
                                    } else {
                                        chosenSeat.add(seat.toUpperCase());
                                        break;
                                    }
                                }
                            }
                        }
                    } else if (round.equals("14.00(ENG)")) {
                        for (int i = 0; i < 12; i++) {
                            for (int j = 0; j < 20; j++) {
                                while (seatConfirm122.contains(seat.toUpperCase())) {
                                    System.out.println("This seat is selected already.");
                                    System.out.print("Please select a new seat :");
                                    seat = kb.next();
                                    if (seatConfirm122.contains(seat.toUpperCase())) {
                                        continue;
                                    } else {
                                        chosenSeat.add(seat.toUpperCase());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (number == 2) {
                if (day.equals("28/08/2023")) {
                    if (round.equals("10.00(TH)")) {
                        for (int i = 0; i < 12; i++) {
                            for (int j = 0; j < 20; j++) {
                                while (seatConfirm211.contains(seat.toUpperCase())) {
                                    System.out.println("This seat is selected already.");
                                    System.out.print("Please select a new seat :");
                                    seat = kb.next();
                                    if (seatConfirm211.contains(seat.toUpperCase())) {
                                        continue;
                                    } else {
                                        chosenSeat.add(seat.toUpperCase());
                                        break;
                                    }
                                }
                            }
                        }
                    } else if (round.equals("14.00(ENG)")) {
                        for (int i = 0; i < 12; i++) {
                            for (int j = 0; j < 20; j++) {
                                while (seatConfirm212.contains(seat.toUpperCase())) {
                                    System.out.println("This seat is selected already.");
                                    System.out.print("Please select a new seat :");
                                    seat = kb.next();
                                    if (seatConfirm212.contains(seat.toUpperCase())) {
                                        continue;
                                    } else {
                                        chosenSeat.add(seat.toUpperCase());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                } else if (day.equals("29/08/2023")) {
                    if (round.equals("10.00(TH)")) {
                        for (int i = 0; i < 12; i++) {
                            for (int j = 0; j < 20; j++) {
                                while (seatConfirm221.contains(seat.toUpperCase())) {
                                    System.out.println("This seat is selected already.");
                                    System.out.print("Please select a new seat :");
                                    seat = kb.next();
                                    if (seatConfirm221.contains(seat.toUpperCase())) {
                                        continue;
                                    } else {
                                        chosenSeat.add(seat.toUpperCase());
                                        break;
                                    }
                                }
                            }
                        }
                    } else if (round.equals("14.00(ENG)")) {
                        for (int i = 0; i < 12; i++) {
                            for (int j = 0; j < 20; j++) {
                                while (seatConfirm222.contains(seat.toUpperCase())) {
                                    System.out.println("This seat is selected already.");
                                    System.out.print("Please select a new seat :");
                                    seat = kb.next();
                                    if (seatConfirm222.contains(seat.toUpperCase())) {
                                        continue;
                                    } else {
                                        chosenSeat.add(seat.toUpperCase());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (number == 3) {
                if (day.equals("28/08/2023")) {
                    if (round.equals("10.00(TH)")) {
                        for (int i = 0; i < 12; i++) {
                            for (int j = 0; j < 20; j++) {
                                while (seatConfirm311.contains(seat.toUpperCase())) {
                                    System.out.println("This seat is selected already.");
                                    System.out.print("Please select a new seat :");
                                    seat = kb.next();
                                    if (seatConfirm311.contains(seat.toUpperCase())) {
                                        continue;
                                    } else {
                                        chosenSeat.add(seat.toUpperCase());
                                        break;
                                    }
                                }
                            }
                        }
                    } else if (round.equals("14.00(ENG)")) {
                        for (int i = 0; i < 12; i++) {
                            for (int j = 0; j < 20; j++) {
                                while (seatConfirm312.contains(seat.toUpperCase())) {
                                    System.out.println("This seat is selected already.");
                                    System.out.print("Please select a new seat :");
                                    seat = kb.next();
                                    if (seatConfirm312.contains(seat.toUpperCase())) {
                                        continue;
                                    } else {
                                        chosenSeat.add(seat.toUpperCase());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                } else if (day.equals("29/08/2023")) {
                    if (round.equals("10.00(TH)")) {
                        for (int i = 0; i < 12; i++) {
                            for (int j = 0; j < 20; j++) {
                                while (seatConfirm321.contains(seat.toUpperCase())) {
                                    System.out.println("This seat is selected already.");
                                    System.out.print("Please select a new seat :");
                                    seat = kb.next();
                                    if (seatConfirm321.contains(seat.toUpperCase())) {
                                        continue;
                                    } else {
                                        chosenSeat.add(seat.toUpperCase());
                                        break;
                                    }
                                }
                            }
                        }
                    } else if (round.equals("14.00(ENG)")) {
                        for (int i = 0; i < 12; i++) {
                            for (int j = 0; j < 20; j++) {
                                while (seatConfirm322.contains(seat.toUpperCase())) {
                                    System.out.println("This seat is selected already.");
                                    System.out.print("Please select a new seat :");
                                    seat = kb.next();
                                    if (seatConfirm322.contains(seat.toUpperCase())) {
                                        continue;
                                    } else {
                                        chosenSeat.add(seat.toUpperCase());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            chosenSeat.add(seat.toUpperCase());
            seatPrice();
            for (int i = 0; i < 6; i++) {
                for (int j = 0; j < 20; j++) {
                    if (seat.equalsIgnoreCase(seats[i][j])) {
                        seatType.add("Deluxe Seat");
                    }
                }
            }
            for (int i = 6; i < 12; i++) {
                for (int j = 0; j < 20; j++) {
                    if (seat.equalsIgnoreCase(seats[i][j])) {
                        seatType.add("Normal Seat");
                    }
                }
            }
        }
    }

    public static void seatPrice() {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 20; j++) {
                if (seat.equalsIgnoreCase(seats[i][j])) {
                    priceTotal += 220;
                }
            }
        }
        for (int i = 6; i < 12; i++) {
            for (int j = 0; j < 20; j++) {
                if (seat.equalsIgnoreCase(seats[i][j])) {
                    priceTotal += 180;
                }
            }
        }
    }

    public static void changeseattoX() {
        for (int r = 0; r < seatConfirm.size(); r++) {
            for (int i = 0; i < 12; i++) {
                for (int j = 0; j < 20; j++) {
                    for (int t = 0; t < 3; t++) {
                        if (number == 1) {
                            if (day.equals("28/08/2023")) {
                                if (round.equals("10.00(TH)")) {
                                    if (seatConfirm.get(r).equalsIgnoreCase(Barbie_1_1[i][j]) || seatConfirm.get(r).equalsIgnoreCase("X")) {
                                        Barbie_1_1[i][j] = "X";
                                    }
                                } else if (round.equals("14.00(ENG)")) {
                                    if (seatConfirm.get(r).equalsIgnoreCase(Barbie_1_2[i][j]) || seatConfirm.get(r).equalsIgnoreCase("X")) {
                                        Barbie_1_2[i][j] = "X";
                                    }
                                }
                            } else if (day.equals("29/08/2023")) {
                                if (round.equals("10.00(TH)")) {
                                    if (seatConfirm.get(r).equalsIgnoreCase(Barbie_2_1[i][j]) || seatConfirm.get(r).equalsIgnoreCase("X")) {
                                        Barbie_2_1[i][j] = "X";
                                    }
                                } else if (round.equals("14.00(ENG)")) {
                                    if (seatConfirm.get(r).equalsIgnoreCase(Barbie_2_2[i][j]) || seatConfirm.get(r).equalsIgnoreCase("X")) {
                                        Barbie_2_2[i][j] = "X";
                                    }
                                }
                            }
                        }
                        if (number == 2) {
                            if (day.equals("28/08/2023")) {
                                if (round.equals("10.00(TH)")) {
                                    if (seatConfirm.get(r).equalsIgnoreCase(TheMeg2_1_1[i][j]) || seatConfirm.get(r).equalsIgnoreCase("X")) {
                                        TheMeg2_1_1[i][j] = "X";
                                    }
                                } else if (round.equals("14.00(ENG)")) {
                                    if (seatConfirm.get(r).equalsIgnoreCase(TheMeg2_1_2[i][j]) || seatConfirm.get(r).equalsIgnoreCase("X")) {
                                        TheMeg2_1_2[i][j] = "X";
                                    }
                                }
                            } else if (day.equals("29/08/2023")) {
                                if (round.equals("10.00(TH)")) {
                                    if (seatConfirm.get(r).equalsIgnoreCase(TheMeg2_2_1[i][j]) || seatConfirm.get(r).equalsIgnoreCase("X")) {
                                        TheMeg2_2_1[i][j] = "X";
                                    }
                                } else if (round.equals("14.00(ENG)")) {
                                    if (seatConfirm.get(r).equalsIgnoreCase(TheMeg2_2_2[i][j]) || seatConfirm.get(r).equalsIgnoreCase("X")) {
                                        TheMeg2_2_2[i][j] = "X";
                                    }
                                }
                            }
                        }
                        if (number == 3) {
                            if (day.equals("28/08/2023")) {
                                if (round.equals("10.00(TH)")) {
                                    if (seatConfirm.get(r).equalsIgnoreCase(LongLiveLove_1_1[i][j]) || seatConfirm.get(r).equalsIgnoreCase("X")) {
                                        LongLiveLove_1_1[i][j] = "X";
                                    }
                                } else if (round.equals("14.00(ENG)")) {
                                    if (seatConfirm.get(r).equalsIgnoreCase(LongLiveLove_1_2[i][j]) || seatConfirm.get(r).equalsIgnoreCase("X")) {
                                        LongLiveLove_1_2[i][j] = "X";
                                    }
                                }
                            } else if (day.equals("29/08/2023")) {
                                if (round.equals("10.00(TH)")) {
                                    if (seatConfirm.get(r).equalsIgnoreCase(LongLiveLove_2_1[i][j]) || seatConfirm.get(r).equalsIgnoreCase("X")) {
                                        LongLiveLove_2_1[i][j] = "X";
                                    }
                                } else if (round.equals("14.00(ENG)")) {
                                    if (seatConfirm.get(r).equalsIgnoreCase(LongLiveLove_2_2[i][j]) || seatConfirm.get(r).equalsIgnoreCase("X")) {
                                        LongLiveLove_2_2[i][j] = "X";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static boolean confirmBooking() {
        System.out.println("You want to confirm booking?(YES/NO)");
        String confirm = kb.next();
        if (confirm.equalsIgnoreCase("YES")) {
            for (int r = 0; r < chosenSeat.size(); r++) {
                for (int i = 0; i < 12; i++) {
                    for (int j = 0; j < 20; j++) {
                        if (number == 1) {
                            if (day.equals("28/08/2023")) {
                                if (round.equals("10.00(TH)")) {
                                    if (chosenSeat.get(r).equalsIgnoreCase(Barbie_1_1[i][j])) {
                                        seatConfirm111.add(chosenSeat.get(r));
                                    } else if (round.equals("14.00(ENG)")) {
                                        if (chosenSeat.get(r).equalsIgnoreCase(Barbie_1_2[i][j])) {
                                            seatConfirm112.add(chosenSeat.get(r));
                                        }
                                    }
                                }
                            } else if (day.equals("29/08/2023")) {
                                if (round.equals("10.00(TH)")) {
                                    if (chosenSeat.get(r).equalsIgnoreCase(Barbie_2_1[i][j])) {
                                        seatConfirm121.add(chosenSeat.get(r));
                                    }
                                } else if (round.equals("14.00(ENG)")) {
                                    if (chosenSeat.get(r).equalsIgnoreCase(Barbie_2_2[i][j])) {
                                        seatConfirm122.add(chosenSeat.get(r));
                                    }
                                }
                            }
                        }
                            if (number == 2) {
                                if (day.equals("28/08/2023")) {
                                    if (round.equals("10.00(TH)")) {
                                        if (chosenSeat.get(r).equalsIgnoreCase(TheMeg2_1_1[i][j])) {
                                            seatConfirm211.add(chosenSeat.get(r));
                                        } else if (round.equals("14.00(ENG)")) {
                                            if (chosenSeat.get(r).equalsIgnoreCase(TheMeg2_1_2[i][j])) {
                                                seatConfirm212.add(chosenSeat.get(r));
                                            }
                                        }
                                    }
                                } else if (day.equals("29/08/2023")) {
                                    if (round.equals("10.00(TH)")) {
                                        if (chosenSeat.get(r).equalsIgnoreCase(TheMeg2_2_1[i][j])) {
                                            seatConfirm221.add(chosenSeat.get(r));
                                        } else if (round.equals("14.00(ENG)")) {
                                            if (chosenSeat.get(r).equalsIgnoreCase(TheMeg2_2_2[i][j])) {
                                                seatConfirm222.add(chosenSeat.get(r));
                                            }
                                        }
                                    }
                                }
                            }
                            if (number == 3) {
                                if (day.equals("28/08/2023")) {
                                    if (round.equals("10.00(TH)")) {
                                        if (chosenSeat.get(r).equalsIgnoreCase(LongLiveLove_1_1[i][j])) {
                                            seatConfirm311.add(chosenSeat.get(r));
                                        } else if (round.equals("14.00(ENG)")) {
                                            if (chosenSeat.get(r).equalsIgnoreCase(LongLiveLove_1_2[i][j])) {
                                                seatConfirm312.add(chosenSeat.get(r));
                                            }
                                        } else if (day.equals("29/08/2023")) {
                                            if (round.equals("10.00(TH)")) {
                                                if (chosenSeat.get(r).equalsIgnoreCase(LongLiveLove_2_1[i][j])) {
                                                    seatConfirm321.add(chosenSeat.get(r));
                                                }
                                            } else if (round.equals("14.00(ENG)")) {
                                                if (chosenSeat.get(r).equalsIgnoreCase(LongLiveLove_2_2[i][j])) {
                                                    seatConfirm322.add(chosenSeat.get(r));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            while (true) {
                System.out.print("Please put your cash: ");
                int cash = kb.nextInt();
                if (cash >= priceTotal) {
                    System.out.println("Thank you for your payment");
                    System.out.println("Change: " + (cash - priceTotal) + " Baht");

                    break;
                } else if (cash < priceTotal) {
                    System.out.println("Cash is not enough.");
                    System.out.println("Please try again.");
                    continue;
                }
            }
            return true;
        } else if (confirm.equalsIgnoreCase("NO")) {
            clearBooking();
        } else {
            System.out.println("Please Confirm Again");
            confirmBooking();
        }
        return false;
    }

    public static void BarbieSeats_1_1() {
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 20; j++) {
                Barbie_1_1[i][j] = Seat.seats[i][j];
            }
        }
    }

    public static void BarbieSeats_1_2() {
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 20; j++) {
                Barbie_1_2[i][j] = Seat.seats[i][j];
            }
        }
    }

    public static void BarbieSeats_2_1() {
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 20; j++) {
                Barbie_2_1[i][j] = Seat.seats[i][j];
            }
        }
    }

    public static void BarbieSeats_2_2() {
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 20; j++) {
                Barbie_2_2[i][j] = Seat.seats[i][j];
            }
        }
    }

    public static void TheMeg2Seats_1_1() {
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 20; j++) {
                TheMeg2_1_1[i][j] = Seat.seats[i][j];
            }
        }
    }

    public static void TheMeg2Seats_1_2() {
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 20; j++) {
                TheMeg2_1_2[i][j] = Seat.seats[i][j];
            }
        }
    }

    public static void TheMeg2Seats_2_1() {
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 20; j++) {
                TheMeg2_2_1[i][j] = Seat.seats[i][j];
            }
        }
    }

    public static void TheMeg2Seats_2_2() {
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 20; j++) {
                TheMeg2_2_2[i][j] = Seat.seats[i][j];
            }
        }
    }

    public static void LongLiveLoveSeats_1_1() {
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 20; j++) {
                LongLiveLove_1_1[i][j] = Seat.seats[i][j];
            }
        }
    }

    public static void LongLiveLoveSeats_1_2() {
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 20; j++) {
                LongLiveLove_1_2[i][j] = Seat.seats[i][j];
            }
        }
    }

    public static void LongLiveLoveSeats_2_1() {
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 20; j++) {
                LongLiveLove_2_1[i][j] = Seat.seats[i][j];
            }
        }
    }

    public static void LongLiveLoveSeats_2_2() {
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 20; j++) {
                LongLiveLove_2_2[i][j] = Seat.seats[i][j];
            }
        }
    }

    public static void clearBooking() {
        seatType.clear();
        Ticket.DateBooking.clear();
        Ticket.TimeBooking.clear();
        chosenSeat.clear();
        priceTotal = 0;
    }

}
